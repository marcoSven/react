import React, { Component } from 'react';
import Test from './Test';
import TestTwo from './TestTwo';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  componentDidMount() {}

  componentWillUnmount() {}
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Test />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <p>
            <Test />
          </p>
          <p>
            <TestTwo />
          </p>
        </header>
      </div>
    );
  }
}

export default App;
