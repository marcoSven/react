import React from 'react';

function Test() {
  const [isThis, setThis] = React.useState(null);

  React.useEffect(() => {
    setThis(false);
    return function cleanup() {};
  });

  return isThis ? '' : 'React Test Not Working - TestTwo.js';
}

export default Test;
