import { useState, useEffect } from 'react';

function Test() {
  const [isThis, setThis] = useState(null);

  useEffect(() => {
    setThis(true);
    return function cleanup() {};
  });

  return isThis ? 'React Test Working - Test.js' : '';
}

export default Test;
