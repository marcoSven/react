
## Settings in here override those in "LSP-typescript/LSP-typescript.sublime-settings"

`{
	"languages": [
		{
			"languageId": "javascript",
			"scopes": [
				"source.js",
			],
			"syntaxes": [
				"Packages/JavaScript/JavaScript.sublime-syntax",
				"Packages/Babel/JavaScript (Babel).sublime-syntax",
			],
		},
		{
			"languageId": "javascriptreact",
			"scopes": [
				"source.js.react",
				"source.jsx"
			],
			"syntaxes": [
				"Packages/User/JS Custom/Syntaxes/React.sublime-syntax",
			],
		},
		{
			"languageId": "typescript",
			"scopes": [
				"source.ts",
			],
			"syntaxes": [
				"Packages/TypeScript Syntax/TypeScript.tmLanguage",
			],
		},
		{
			"languageId": "typescriptreact",
			"scopes": [
				"source.tsx",
			],
			"syntaxes": [
				"Packages/TypeScript Syntax/TypeScriptReact.tmLanguage",
			],
		},
	],
	"initializationOptions": {},
	"settings": {},
}`


## LSP

`{
  "show_code_actions_bulb": true,
  "auto_show_diagnostics_panel": "never",
  "show_diagnostics_severity_level": 4,
  "show_diagnostics_count_in_view_status": true,
  "show_symbol_action_links": true,
  "show_references_in_quick_panel": true,
  "clients":
  {
    "flow":
    {
      "command": ["flow", "lsp"],
      "scopes": ["source.js.react"],
      "syntaxes": ["Packages/User/JS Custom/Syntaxes/React.sublime-syntax", "Packages/Babel/JavaScript (Babel).sublime-syntax", "Packages/JavaScript/JavaScript.sublime-syntax"],
      "languageId": "javascript",
      "enabled": false,
    },
  		"lsp-typescript":
      {
        "enabled": true,
     },
		"lsp-vue":
		{
			"enabled": false,
		},
		"lsp-css":
   {
     "enabled": false
   },
 },
}`






This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
